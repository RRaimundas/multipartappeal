package lt.vpb.appeals;



import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AppealApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        try {
            AppealDataToTxt.execute();

        } catch (IOException ex) {
            Logger.getLogger(AppealApp.class.getName()).log(Level.SEVERE, null, ex);
        }

   System.out.println("uploading PANZ FILE---------------------------------------------------");

        Oposition PANZ = new Oposition();
        PANZ.MultiPartOpposition();

    System.out.println("uploading PATZ FILE---------------------------------------------------");

        Oposition_tarp PATZ = new Oposition_tarp();
        PATZ.MultiPartOpposition_tapt();

        System.out.println("Sending Email to ---------------------------------------------------");

        smtp_send sendMail = new smtp_send();
        sendMail.sendEmail();



    }

}
