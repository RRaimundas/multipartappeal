package lt.vpb.appeals;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.*;
import java.io.PrintWriter;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dputna
 */

public class AppealDataToTxt {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    static final String DB_URL = "jdbc:mysql://10.30.1.41:3306/backoffice";
    static final String DB_URL_Activiti = "jdbc:mysql://10.30.1.41:3306/activiti";
    static Map<String, String> TaskValue = new HashMap<>();

    //  Database credentials
    static final String USER = "cti";
    static final String PASS = "2crJKhMXdBkmzoZvQT3XyWsxMj";

    //-----------------------------------------------------------------------------------------
    public static void PrintToFileN(PrintWriter out, String application_nr, String apealOppo_nr, String decition_nr, Date dec_date, String decition, String legal, String goods_and_services, Clob des_text_clob, String curent_status, int typeOppOrApeal, String iddosier) throws SQLException {

        String desition_text = null;

        if (legal != null) {
            String[] split = legal.split(",");
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < split.length; j++) {
                sb.append(setGroundValue(split[j]));
                if (j != split.length - 1) {
                    sb.append(";");
                }
            }
            legal = sb.toString();
        }

        if (des_text_clob != null) {
            desition_text = des_text_clob.getSubString(1, (int) des_text_clob.length());
        } //transformuoja tekstą
        else {
            desition_text = "";
        }


        if (typeOppOrApeal == 1) {
            out.print(application_nr.trim() + "		");//1
            out.print(apealOppo_nr.trim().replaceAll(" ", "").replaceAll("ANZ-", "") + "	");//2
        } else if (typeOppOrApeal == 2) {
            out.print(application_nr.trim() + "	");//1
            out.print(apealOppo_nr.trim().replaceAll(" ", "").replaceAll("PNZ-", "") + "		");
        }


        if (decition_nr != null && decition_nr.contains("2Ap-")) {

            out.print(decition_nr.trim().replace(" ", "").replace("2Ap-", "") + "	");
        }//3
        else {
            out.print("	");
        }

        if (dec_date != null) {
            out.print((dec_date.toString()).replace('-', '.').trim() + "	");//4
        } else {
            out.print("	");
        }

        if (decition != null) {
            if (typeOppOrApeal == 1) {
                decition = setDesValue(decition);
                out.print(decition + "	");
            } else if (typeOppOrApeal == 2) {
                decition = setProtestDesValue(decition);
                out.print(decition + "	");
            }
        }//5
        else {
            out.print("	");
        }

        if (legal != null) {
            out.print(legal.trim() + "	");
        } else {
            out.print(" ");
        } //6

        if (goods_and_services != null) {
            out.println(goods_and_services.trim());
        } else {
            out.println("");
        }//7

        if (desition_text != null || desition_text != "") {
            out.print(desition_text.trim() + "\n");
        } //8
        else {
            out.print(" \n");
        }

        // out.print("šioje vietoje turetu būt sprendimo tekstas \n");
        if (curent_status != null) {
            if (typeOppOrApeal == 1) {
                out.print("	" + setCurentStValue(curent_status, iddosier) + " \n");
            } else if (typeOppOrApeal == 2) {
                out.print("	" + setProtestCurentStValue(curent_status, iddosier) + " \n");
            }
        } else {
            out.print("	\n");
        } //9
        out.println("<END OF RECORD> ");//11 */
    }

    //-----------------------------------------------
    public static void PrintToFileT(PrintWriter out, String application_nr, String apealOppo_nr, String decition_nr, Date dec_date, String decition, String legal, String goods_and_services, Clob des_text_clob, String curent_status, int typeOppOrApeal, String iddosier) throws SQLException {

        String desition_text = null;

        if (legal != null) {
            String[] split = legal.split(",");
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < split.length; j++) {
                sb.append(setGroundValue(split[j]));
                if (j != split.length - 1) {
                    sb.append(";");
                }
            }
            legal = sb.toString();
        }

        if (des_text_clob != null) {
            desition_text = des_text_clob.getSubString(1, (int) des_text_clob.length());
        } //transformuoja tekstą
        else {
            desition_text = "";
        }


        if (typeOppOrApeal == 1) {
            out.print(application_nr.trim() + "		");//1
            out.print(apealOppo_nr.trim().replaceAll(" ", "").replaceAll("ATZ-", "") + "	");//2
        } else if (typeOppOrApeal == 2) {
            out.print(application_nr.trim() + "	");//1
            out.print(apealOppo_nr.trim().replaceAll(" ", "").replaceAll("PTZ-", "") + "		");

        }


        if (decition_nr != null) {
            out.print(decition_nr.trim().replace(" ", "").replace("2Ap-", "") + "	");
        }//3
        else {
            out.print("	");
        }

        if (dec_date != null) {
            out.print((dec_date.toString()).replace('-', '.').trim() + "	");//4
        } else {
            out.print("	");
        }

        if (decition != null) {
            if (typeOppOrApeal == 1) {
                decition = setDesValue(decition);
                out.print(decition + "	");
            } else if (typeOppOrApeal == 2) {
                decition = setProtestDesValue(decition);
                out.print(decition + "	");
            }
        }//5
        else {
            out.print("	");
        }

        if (legal != null) {
            out.print(legal.trim() + "	");
        } else {
            out.print(" ");
        } //6

        if (goods_and_services != null) {
            out.println(goods_and_services.trim());
        } else {
            out.println("");
        }//7

        if (desition_text != null || desition_text != "") {
            out.print(desition_text.trim() + "\n");
        } //8
        else {
            out.print(" \n");
        }

        // out.print("šioje vietoje turetu būt sprendimo tekstas \n");
        if (curent_status != null) {
            if (typeOppOrApeal == 1) {
                out.print("	" + setCurentStValue(curent_status, iddosier) + " \n");
            } else if (typeOppOrApeal == 2) {
                out.print("	" + setProtestCurentStValue(curent_status, iddosier) + " \n");
            }
        } else {
            out.print("	\n");
        } //9
        out.println("<END OF RECORD> ");//11 */

    }

    //------------------------------------------------------------------
    public static String setDesValue(String des) // grazina indeksa
    {
        String resultInt = "";

        switch (des.toUpperCase().trim()) {
            case "GRANTED":
                resultInt = "1";
                break;
            case "PARTIALLY_GRANTED":
                resultInt = "2";
                break;
            case "REJECTED":
                resultInt = "3";
                break;
        }
        return resultInt;
    }

    //-----------------------------------------------
    public static String setProtestDesValue(String des) // grazina indeksa
    {
        String resultInt = "";

        switch (des.toUpperCase().trim()) {
            case "GRANTED":
                resultInt = "4";
                break;
            case "PARTIALLY_GRANTED":
                resultInt = "5";
                break;
            case "REJECTED":
                resultInt = "6";
                break;
        }
        return resultInt;
    }

    //----------------------------------------------------------------
    public static String setProtestCurentStValue(String status, String iddosier) // grazina indeksa
    {
        String resultInt = "";

        switch (status.toUpperCase().trim()) {
            case "INADMISIBLE":
                resultInt = "1";
                break;
            case "PENDING":
                resultInt = setPending(iddosier);
                break;
            case "SUSPENDED":
                resultInt = "3";
                break;
            case "GRANTED":
                resultInt = "4";
                break;
            case "PARTIALLY_GRANTED":
                resultInt = "4";
                break;
            case "REJECTED":
                resultInt = "4";
                break;
            case "WITHDRAW":
                resultInt = "5";
                break;
        }
        return resultInt;
    }

    //-----------------------------------------------------------------------
    public static String setPending(String iddossier) // grazina indeksa
    {
        String resultInt = "10";

        if (TaskValue.get(iddossier) == null) {
            return resultInt;
        } else {
            switch (TaskValue.get(iddossier)) {
                case "Opposition suspended":
                    resultInt = "3";
                    break;
                case "Opposition hearing":
                    resultInt = "2";
                    break;
                case "Waiting for a justified arguments from opponent":
                    resultInt = " 7";
                    break;
                case "Waiting for a reply to justified arguments from owner":
                    resultInt = "8";
                    break;
                case "Waiting for a motivated reply from owner":
                    resultInt = "6";
                    break;
                case "Send opposition decision letter":
                    resultInt = "4";
                    break;
                case "Appeal hearing":
                    resultInt = "2";
                    break;
                default:
                    resultInt = "10";
                    break;
            }
            return resultInt;
        }
    }

    //----------------------------------------------------------------
    public static String setCurentStValue(String status, String iddosier) // grazina indeksa
    {
        String resultInt = "";

        switch (status.toUpperCase().trim()) {
            case "INADMISIBLE":
                resultInt = "1";
                break;
            case "PENDING":
                resultInt = setPending(iddosier);
                break;
            case "SUSPENDED":
                resultInt = "3";
                break;
            case "GRANTED":
                resultInt = "4";
                break;
            case "PARTIALLY_GRANTED":
                resultInt = "4";
                break;
            case "REJECTED":
                resultInt = "4";
                break;
            case "WITHDRAW":
                resultInt = "5";
                break;
        }
        return resultInt;
    }

    //----------------------------------------------------------------
    public static String setGroundValue(String ground) // grazina indeksa
    {
        String resultInt = "";
        switch (ground.toUpperCase().trim()) {
            case "6.1.1":
                resultInt = "1";
                break;
            case "6.1.2":
                resultInt = "2";
                break;
            case "6.1.3":
                resultInt = "3";
                break;
            case "6.1.4":
                resultInt = "4";
                break;
            case "6.1.5":
                resultInt = "5";
                break;
            case "6.1.6":
                resultInt = "6";
                break;
            case "6.1.7":
                resultInt = "7";
                break;
            case "6.1.8":
                resultInt = "8";
                break;
            case "6.1.9":
                resultInt = "9";
                break;
            case "6.1.10":
                resultInt = "10";
                break;
            case "6.1.11":
                resultInt = "11";
                break;
            case "6.1.12":
                resultInt = "12";
                break;
            case "6.2":
                resultInt = "13";
                break;
            case "7.1.1":
                resultInt = "14";
                break;
            case "7.1.2":
                resultInt = "15";
                break;
            case "7.1.3":
                resultInt = "16";
                break;
            case "7.1.4":
                resultInt = "17";
                break;
            case "7.1.5":
                resultInt = "18";
                break;
            case "7.1.6":
                resultInt = "19";
                break;
            case "7.1.7":
                resultInt = "20";
                break;
            case "7.1.8":
                resultInt = "21";
                break;
            case "7.3":
                resultInt = "22";
                break;
            default:
                resultInt = "";
                break;
        }
        return resultInt;
    }

    //=====================================================================================================
    public static void Apeal(String sql, String txtFile, char typeNorT) {
        Connection conn = null;
        Statement stmt = null;

        try {
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            System.out.println("Jungiamasi prie duomenu bazes (Apeal)");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Prisijungta");

            //STEP 4: Execute a query
            System.out.println("Vykdoma uzklausa...");
            stmt = conn.createStatement();

            int i = 0;

            ResultSet rs = stmt.executeQuery(sql);
            PrintWriter out = new PrintWriter(txtFile, "Windows-1257"); //
            //STEP 5: Extract data from result set

            while (rs.next()) {
                //Retrieve by column name
                i++;
                //--------------------------------------------------------------------
                String iddossierrs = rs.getString("DAPPEAL.IDDOSSIER");
                String application_nr = rs.getString("DDOSSIER.REFAPPLICATIONNUMBER");
                String apeal_nr = rs.getString("DDS.REFAPPLICATIONNUMBER"); // apeliacijos numeris
                String decition_nr = rs.getString("NRCASENUMBER");
                Date dec_date = rs.getDate("DTDECISIONCAMEINTOEFFECT"); // data
                String decition = rs.getString("TYDECISIONTAKEN");
                String legal = rs.getString("GROUNDS");
                String goods_and_services = rs.getString("TXTANNOTATION");
                Clob des_text_clob = rs.getClob("COMMENTS"); // isrenka teksta
                String curent_status = rs.getString("DDS.STCURRENTSTATUS");

                if (typeNorT == 'N') {
                    PrintToFileN(out, application_nr, apeal_nr, decition_nr, dec_date, decition, legal, goods_and_services, des_text_clob, curent_status, 1, iddossierrs);
                } else if (typeNorT == 'T') {
                    PrintToFileT(out, application_nr, apeal_nr, decition_nr, dec_date, decition, legal, goods_and_services, des_text_clob, curent_status, 1, iddossierrs);
                }


                //------------------------------------------------------------------------
            }
            System.out.println("Duomenu rinkiniu kiekis " + i);
            out.close();
            rs.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }

    //=====================================================================================================
    public static void Oposition(String sql, Map classValue, String txtFile, char typeNorT) {
        Connection conn = null;
        Statement stmt = null;

        try {
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            System.out.println("Jungiamasi prie duomenu bazes (Opp)");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Prisijungta");

            //STEP 4: Execute a query
            System.out.println("Vykdoma uzklausa...");
            stmt = conn.createStatement();

            int i = 0;

            ResultSet rs = stmt.executeQuery(sql);
            PrintWriter out = new PrintWriter(txtFile, "Windows-1257"); //Realei aplinkai
            //STEP 5: Extract data from result set

            while (rs.next()) {
                //Retrieve by column name
                i++;
                //--------------------------------------------------------------------
                String idOpp = rs.getString("DOPPOSITION.IDDOSSIER");
                String application_nr = rs.getString("DDOSSIER.REFAPPLICATIONNUMBER");
                String opp_nr = rs.getString("DDS.REFAPPLICATIONNUMBER"); // apeliacijos numeris
                String decition_nr = rs.getString("NRCASENUMBER");
                Date dec_date = rs.getDate("DTDECISIONCAMEINTOEFFECT"); // data
                String decition = rs.getString("TYDECISIONTAKEN");
                String legal = rs.getString("LAWARTICLES");
                String goods_and_services = (String) classValue.get(idOpp);
                Clob des_text_clob = rs.getClob("DOPPOSITIONDECISION.COMMENTS"); // isrenka teksta
                String curent_status = rs.getString("DDS.STCURRENTSTATUS");

                if (typeNorT == 'N') {
                    PrintToFileN(out, application_nr, opp_nr, decition_nr, dec_date, decition, legal, goods_and_services, des_text_clob, curent_status, 2, idOpp);
                } else if (typeNorT == 'T') {
                    PrintToFileT(out, application_nr, opp_nr, decition_nr, dec_date, decition, legal, goods_and_services, des_text_clob, curent_status, 2, idOpp);
                }
                //------------------------------------------------------------------------
            }
            System.out.println("Duomenu rinkiniu kiekis " + i);
            out.close();
            rs.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }

    //==============================================================================
    public static Map setStrigValue(String sql) {
        String temp = null;
        String oldId = "1";
        Map<String, String> classValue = new HashMap<>();
        classValue.clear();

        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);


            System.out.println("Jungiamasi prie duomenu bazes (opp Gr)");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Prisijungta");


            stmt = conn.createStatement();
            System.out.println("Vykdoma uzklausa...");
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {

                String oppId = rs.getString("DOPPOSITION.IDDOSSIER");
                String setClass = rs.getString("NRCLASSNUMBER");

                if (!oppId.equals(oldId)) {
                    classValue.put(oppId, setClass);
                } else {
                    temp = classValue.get(oppId) + ";" + setClass;
                    classValue.put(oppId, temp);
                }
                oldId = oppId;

            }
            System.out.println("Klases sutvarkytos");
            rs.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return classValue;
    }

    //---------------------------------------------
    public static Map setValueFromTask() {
        String oldId = "1";
        Map<String, String> classValue = new HashMap<>();
        classValue.clear();

        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Jungiamasi prie duomenu bazes (Activiti)");
            conn = DriverManager.getConnection(DB_URL_Activiti, USER, PASS);
            System.out.println("Prisijungta");


            stmt = conn.createStatement();
            System.out.println("Vykdoma uzklausa...");
            String sql = "SELECT DOSSIER_ID_, NAME_ FROM activiti.ACT_BO_TASK\n" +
                    "left join activiti.ACT_RU_TASK on activiti.ACT_BO_TASK.TASK_ID_=activiti.ACT_RU_TASK.ID_\n" +
                    "where (activiti.ACT_BO_TASK.DOSSIER_TYPE_ like '%TrademarkAppeal%' or activiti.ACT_BO_TASK.DOSSIER_TYPE_ like '%TrademarkOpposition%') AND NAME_ IS NOT NULL order by DOSSIER_ID_, TASK_ID_ DeSC;";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {

                String oppId = rs.getString("DOSSIER_ID_");
                String activiti = rs.getString("NAME_");


                if (!oppId.equals(oldId) && (activiti != null || !"".equals(activiti))) {
                    classValue.put(oppId, activiti);
                    // System.out.print(oldId+" -> ");
                    // System.out.println(classValue.get(oppId));
                }
                oldId = oppId;

            }
            System.out.println("Task'ai priskirti");
            System.out.println("---------------------------------------------");
            rs.close();

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return classValue;
    }

    //-------------------------------------------------------------------------------
    public static void mergeFiles(String fileName1, String fileName2, String FileOutput) throws IOException {
        ArrayList<String> list = new ArrayList<String>();
        BufferedReader br = null;
        BufferedReader r = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName1), "Windows-1257"));
            r = new BufferedReader(new InputStreamReader(new FileInputStream(fileName2), "Windows-1257"));
            String s1 = null;
            String s2 = null;

            while ((s1 = br.readLine()) != null) {
                list.add(s1);
            }
            while ((s2 = r.readLine()) != null) {
                list.add(s2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
            r.close();
        }


        BufferedWriter writer = null;
        writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FileOutput), "Windows-1257"));
        String listWord;
        for (int i = 0; i < list.size(); i++) {
            listWord = list.get(i);
            writer.write(listWord);
            writer.write("\n");
        }

        writer.close();
    }

    //------------------------------------------------------------------------------
    public static void execute() throws IOException {

        String sqlNrClassnumber = null;
        Map<String, String> classValue = new HashMap<>();

        TaskValue = setValueFromTask();


        String sql = "select DISTINCT DAPPEAL.IDDOSSIER, DDOSSIER.REFAPPLICATIONNUMBER, DDS.REFAPPLICATIONNUMBER, NRCASENUMBER, DTDECISIONCAMEINTOEFFECT, TYDECISIONTAKEN, GROUNDS, TXTANNOTATION, COMMENTS, DDS.STCURRENTSTATUS from DAPPEAL\n" +
                "left join DAPPEALDECISION on DAPPEALDECISION.IDDOSSIER = DAPPEAL.IDDOSSIER\n" +
                "left join DDOSSIER on DAPPEAL.IDAPPEALEDDOSSIER=DDOSSIER.IDDOSSIER\n" +
                "left join DDOSSIER AS DDS on DAPPEAL.IDDOSSIER=DDS.IDDOSSIER\n" +
                "left join DANNOTATION on DANNOTATION.IDDOSSIER= DAPPEAL.IDDOSSIER\n" +
                "where (NMTITTLE!='Remarks' or NMTITTLE is null) and DDS.REFAPPLICATIONNUMBER like '%ANZ%' order by IDDOSSIER ASC";

        Apeal(sql, "ANZ.txt", 'N'); //nacionalines

        System.out.println("\n-------------------------------------------");

        sql = "select DISTINCT DAPPEAL.IDDOSSIER, DDOSSIER.REFAPPLICATIONNUMBER, DDS.REFAPPLICATIONNUMBER, NRCASENUMBER, DTDECISIONCAMEINTOEFFECT, TYDECISIONTAKEN, GROUNDS, TXTANNOTATION, COMMENTS, DDS.STCURRENTSTATUS from DAPPEAL\n" +
                "left join DAPPEALDECISION on DAPPEALDECISION.IDDOSSIER = DAPPEAL.IDDOSSIER\n" +
                "left join DDOSSIER on DAPPEAL.IDAPPEALEDDOSSIER=DDOSSIER.IDDOSSIER\n" +
                "left join DDOSSIER AS DDS on DAPPEAL.IDDOSSIER=DDS.IDDOSSIER\n" +
                "left join DANNOTATION on DANNOTATION.IDDOSSIER= DAPPEAL.IDDOSSIER\n" +
                "where (NMTITTLE!='Remarks' or NMTITTLE is null) and DDS.REFAPPLICATIONNUMBER like '%ATZ%' order by IDDOSSIER ASC";

        Apeal(sql, "ATZ.txt", 'T'); //tarptautines

        System.out.println("\n-------------------------------------------");

        sql = "select distinct DOPPOSITION.IDDOSSIER, DDOSSIER.REFAPPLICATIONNUMBER, DDS.REFAPPLICATIONNUMBER, NRCASENUMBER, DTDECISIONCAMEINTOEFFECT, TYDECISIONTAKEN, DGROUND.LAWARTICLES, DOPPOSITIONDECISION.COMMENTS, DDS.STCURRENTSTATUS from DOPPOSITION\n" +
                "left join DOPPOSITIONDECISION on DOPPOSITIONDECISION.IDDOSSIER = DOPPOSITION.IDDOSSIER\n" +
                "left join DDOSSIER on DOPPOSITION.IDOPPOSEDDOSSIER=DDOSSIER.IDDOSSIER\n" +
                "left join DDOSSIER AS DDS on DOPPOSITION.IDDOSSIER=DDS.IDDOSSIER\n" +
                "left join DANNOTATION on DANNOTATION.IDDOSSIER= DOPPOSITION.IDDOSSIER\n" +
                "left join DGROUND on DOPPOSITION.IDDOSSIER = DGROUND.IDDOSSIER\n" +
                "where (DANNOTATION.NMTITTLE is null or DANNOTATION.NMTITTLE!='Remarks')and (NRCASENUMBER is null or NRCASENUMBER like '%2Ap%') and DDS.REFAPPLICATIONNUMBER like '%PNZ%' ORDER BY IDDOSSIER;";

        sqlNrClassnumber = "select DISTINCT DOPPOSITION.IDDOSSIER, DDOSSIER.REFAPPLICATIONNUMBER, DCLASSDESCRIPTION.NRCLASSNUMBER from DOPPOSITION\n" +
                "left join DDOSSIER on DOPPOSITION.IDDOSSIER=DDOSSIER.IDDOSSIER\n" +
                "left join DCLASSDESCRIPTION on DOPPOSITION.IDDOSSIER = DCLASSDESCRIPTION.IDDOSSIER\n" +
                "where DDOSSIER.REFAPPLICATIONNUMBER like '%PNZ%' ORDER BY DOPPOSITION.IDDOSSIER asc,DCLASSDESCRIPTION.NRCLASSNUMBER;";
        classValue = setStrigValue(sqlNrClassnumber);

        Oposition(sql, classValue, "PNZ.txt", 'N');

        classValue.clear();
        System.out.println("\n-------------------------------------------");

        sql = "select distinct DOPPOSITION.IDDOSSIER, DDOSSIER.REFAPPLICATIONNUMBER, DDS.REFAPPLICATIONNUMBER, NRCASENUMBER, DTDECISIONCAMEINTOEFFECT, TYDECISIONTAKEN, DGROUND.LAWARTICLES, DOPPOSITIONDECISION.COMMENTS, DDS.STCURRENTSTATUS from DOPPOSITION\n" +
                "left join DOPPOSITIONDECISION on DOPPOSITIONDECISION.IDDOSSIER = DOPPOSITION.IDDOSSIER\n" +
                "left join DDOSSIER on DOPPOSITION.IDOPPOSEDDOSSIER=DDOSSIER.IDDOSSIER\n" +
                "left join DDOSSIER AS DDS on DOPPOSITION.IDDOSSIER=DDS.IDDOSSIER\n" +
                "left join DANNOTATION on DANNOTATION.IDDOSSIER= DOPPOSITION.IDDOSSIER\n" +
                "left join DGROUND on DOPPOSITION.IDDOSSIER = DGROUND.IDDOSSIER\n" +
                "where (DANNOTATION.NMTITTLE is null or DANNOTATION.NMTITTLE!='Remarks')and (NRCASENUMBER is null or NRCASENUMBER like '%2Ap%') and DDS.REFAPPLICATIONNUMBER like '%PTZ%' ORDER BY DOPPOSITION.IDDOSSIER;";

        sqlNrClassnumber = "select DISTINCT DOPPOSITION.IDDOSSIER, DDOSSIER.REFAPPLICATIONNUMBER, DCLASSDESCRIPTION.NRCLASSNUMBER from DOPPOSITION\n" +
                "left join DDOSSIER on DOPPOSITION.IDDOSSIER=DDOSSIER.IDDOSSIER\n" +
                "left join DCLASSDESCRIPTION on DOPPOSITION.IDDOSSIER = DCLASSDESCRIPTION.IDDOSSIER\n" +
                "where DDOSSIER.REFAPPLICATIONNUMBER like '%PNZ%' ORDER BY DOPPOSITION.IDDOSSIER asc,DCLASSDESCRIPTION.NRCLASSNUMBER;";
        classValue = setStrigValue(sqlNrClassnumber);

        Oposition(sql, classValue, "PTZ.txt", 'T');
        System.out.println("\n-------------------------------------------");

        mergeFiles("PTZ.txt", "ATZ.txt", "PATZ.txt");
        mergeFiles("PNZ.txt", "ANZ.txt", "PANZ.txt");

        System.out.println("Failai sugeneruoti\n isvalomi nereikalingi failai.");

        File PTZ = new File("PTZ.txt");
        PTZ.deleteOnExit();
        File PNZ = new File("PNZ.txt");
        PNZ.deleteOnExit();
        File ANZ = new File("ANZ.txt");
        ANZ.deleteOnExit();
        File ATZ = new File("ATZ.txt");
        ATZ.deleteOnExit();
    }
}
