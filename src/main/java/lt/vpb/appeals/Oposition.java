package lt.vpb.appeals;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import java.io.File;
import java.io.IOException;

public class Oposition  {


    public static void MultiPartOpposition() throws
            ClientProtocolException, IOException {

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(Constant.OpositionURL);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        builder.addBinaryBody("file1", new File(Constant.OpositionPANZ),
                ContentType.APPLICATION_OCTET_STREAM, "PANZ.txt");

        httpPost.addHeader("Authorization", "Basic xxxxx");
        httpPost.addHeader("Referer", Constant.OpUploadURL);


        HttpEntity multipart = builder.build();
        httpPost.setEntity(multipart);

        CloseableHttpResponse response = client.execute(httpPost);
        System.out.println(response);

    }
}
